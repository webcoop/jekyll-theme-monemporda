# [WIP] jekyll-theme-monemporda

[![Gem Version](https://badge.fury.io/rb/jekyll-theme-monemporda.png)](https://badge.fury.io/rb/jekyll-theme-monemporda)


This is the jekyll-theme for www.monemporda.org

It includes:

- jekyll-feed
- jekyll-seo-tag
- bootstrap 4
- jQuery 3


## Installation

Add this line to your Jekyll site's `Gemfile`:

```ruby
gem "jekyll-theme-monemporda"
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: jekyll-theme-monemporda
```

And then execute:

    $ bundle install


## Usage

There are some templates available:

- `page`
- `post`
- `collection`
- `gallery`
- `contact`

Text rendered in footer comes from a file `_data/site.yml`:

```yaml
footer:
  - section:
      title: Section One
      body: >
        Lorem ipsum dolor sit amet.
  - section:
      title: Section two
      body: >
        Possimus unde eos vitae eius quasi saepe.  
```

## Development

To set up your environment to develop this theme, run `bundle install`.

If you want to update vendor sass or js (`bootstrap` or `jQuery`) after bundle install run `rake copy`.

## License

The theme is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
