# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-monemporda"
  spec.version       = "0.1.5"
  spec.authors       = ["Agustí B.R."]
  spec.email         = ["hola@agusti.cat"]

  spec.summary       = %q{a jekyll theme for Món Empordà with seo, bootstrap 4 and templates}
  spec.homepage      = "https://monemporda.cat"
  spec.license       = "MIT"

  spec.metadata["plugin_type"] = "theme"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_runtime_dependency "jekyll", "~> 4.0"
  spec.add_runtime_dependency "jekyll-feed"
  spec.add_runtime_dependency "jekyll-seo-tag"

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 12.0"
  spec.add_development_dependency "jquery-rails", "~> 4.3.3"
  spec.add_development_dependency "bootstrap", "~> 4.6.2.1"
end
